FROM node:10.16.0-alpine as node
RUN addgroup -g 82 www-data  \
    && adduser -u 82 -G www-data -s /bin/sh -D www-data && mkdir -p /tmp/front/node_modules
WORKDIR /tmp/front
COPY package*.json ./
COPY ./resources/ ./resources/
COPY webpack.mix.js ./
RUN npm install \
    && npm run prod \
    && ls | grep -v public | xargs rm -rf && chown 82: -R /tmp/front

# #######
FROM composer:1.9.3 AS composer
WORKDIR /tmp/composer
COPY composer.* ./

COPY ./database/ ./database/
ARG APP_ENV
RUN composer global require hirak/prestissimo \
    && if [ "${APP_ENV}" = "production" ]; then \
    composer install --prefer-dist --no-scripts --no-dev --no-autoloader \
    && composer dump-autoload --no-scripts --no-dev --optimize -o \
    ;else \
        composer install --prefer-dist --no-scripts --no-autoloader \
        && composer dump-autoload --no-scripts --optimize -o \
    ;fi \
        && composer global remove hirak/prestissimo \
        && ls | grep -v vendor | xargs rm -rf

########
FROM php:7.4.4-fpm-alpine

RUN apk --no-cache add shadow && \
    usermod -u 82 www-data && \
    groupmod -g 82 www-data \
    && apk add --no-cache libzip-dev libpng-dev zip gd php7-pdo_pgsql libpq postgresql-dev \
    && docker-php-ext-install pdo pdo_pgsql
    && mkdir -p /usr/src/php/ext/redis \
    && curl -L https://github.com/phpredis/phpredis/archive/$PHPREDIS_VERSION.tar.gz | tar xvz -C /usr/src/php/ext/redis --strip 1 \
    && echo 'redis' >> /usr/src/php-available-exts \
    && docker-php-ext-install redis \
    && apk add php7-pecl-redis

COPY --from=node /tmp/front /tmp/front
COPY --from=composer /tmp/composer /tmp/composer

WORKDIR /application
ADD . .
RUN ls -la
RUN chown -R www-data: /application/public/ /application/bootstrap/ /application/storage/ \
    && mkdir -p /application/vendor /var/log/supervisor/ && cp -rup /tmp/front/* ./vendor \
    && chown -R www-data: /application/public/index.php  /application/bootstrap/ /application/storage